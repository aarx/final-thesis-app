package kasper.argo.testapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import kasper.argo.testapp.R;
import kasper.argo.testapp.service.GPSTracker;

/**
 * Created by AARX on 18.05.2016.
 */
public class LogActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

    }

    public void deleteLog(View view) {

        /** Getting Cache Directory */
        File dir = getBaseContext().getFilesDir();

        /** Getting a reference to temporary file, if created earlier */
        File tempFile = new File(dir.getPath() + "/" + GPSTracker.FILE_NAME) ;

        FileWriter writer = null;
        try {
            writer = new FileWriter(tempFile);

            /** Saving the contents to the file*/
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write("");

            TextView posContent = (TextView) findViewById(R.id.log_file_content);

            if(posContent != null) {
                posContent.setText("");
            }


            bufferedWriter.close();

            /** Closing the writer object */
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void copyToPublic(View view) {
        /** Getting Cache Directory */
        File dir = getBaseContext().getFilesDir();

        File newDir =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);

        /** Getting a reference to temporary file, if created earlier */
        File tempFile = new File(dir.getPath() + "/" + GPSTracker.FILE_NAME) ;
        FileOutputStream out = null;

        InputStream in;
        try {
            in = new BufferedInputStream(new FileInputStream(tempFile));
            out = new FileOutputStream(newDir.getPath() + "/log.txt");
            copyFile( in , out); in .close(); in = null;
            out.flush();
            out.close();
            out = null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void copyFile(InputStream in , OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in .read(buffer)) != -1) {
            out.write(buffer, 0, read);

        }
    }

    public void showLog(View view) {
        /** Getting Cache Directory */
        File dir = getBaseContext().getFilesDir();

        /** Getting a reference to temporary file, if created earlier */
        File tempFile = new File(dir.getPath() + "/" + GPSTracker.FILE_NAME) ;

        /** Getting reference to edittext et_content */
        TextView posContent = (TextView) findViewById(R.id.log_file_content);

        String strLine = "";
        StringBuilder text = new StringBuilder();

        /** Reading contents of the temporary file, if already exists */
        try {
            FileReader fReader = new FileReader(tempFile);
            BufferedReader bReader = new BufferedReader(fReader);

            /** Reading the contents of the file , line by line */
            while( (strLine=bReader.readLine()) != null  ){
                    text.append(strLine + "\n");

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        /** Setting content from file */
        posContent.setText(text);
    }
}
