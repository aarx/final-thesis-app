package kasper.argo.testapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import java.io.File;

import kasper.argo.testapp.R;
import kasper.argo.testapp.helper.WriteToFile;
import kasper.argo.testapp.service.GPSTracker;

/**
 * Created by AARX on 16.05.2016.
 */
public class CameraActivity extends AppCompatActivity {

    Context mContext;

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        mContext = this;

    }

    @Override
    public void onResume(){
        super.onResume();
        setContentView(R.layout.activity_camera);

    }

    public void takePhoto(View view) {
        long startTime = System.currentTimeMillis();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
        long endTime = System.currentTimeMillis();

        File dir = mContext.getFilesDir();
        File file = new File(dir.getPath() + "/" + GPSTracker.FILE_NAME) ;

        String message = "\n[CAMERA]";
        message += " [start=" + startTime + ", end=" + endTime + ", end-start=" + (endTime-startTime) + "]";

        // Writes to file
        WriteToFile wtf = new WriteToFile(mContext, file);
        wtf.writeToFile(message);
    }


}
