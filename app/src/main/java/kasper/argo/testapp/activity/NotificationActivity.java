package kasper.argo.testapp.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.joda.time.DateTime;

import java.io.File;

import kasper.argo.testapp.R;
import kasper.argo.testapp.dialog.CustomDialog;
import kasper.argo.testapp.helper.WriteToFile;
import kasper.argo.testapp.service.GPSTracker;

/**
 * Created by AARX on 16.05.2016.
 */
public class NotificationActivity extends AppCompatActivity {

    Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mContext = this;

    }

    @Override
    public void onResume(){
        super.onResume();
        setContentView(R.layout.activity_notification);

    }

     public void openConfirm(View view) {
         long startTime = System.currentTimeMillis();
         CustomDialog dialog = new CustomDialog();
         dialog.show(this.getFragmentManager(), "ConfirmFragment");
         long endTime = System.currentTimeMillis();

         File dir = mContext.getFilesDir();
         File file = new File(dir.getPath() + "/" + GPSTracker.FILE_NAME) ;

         // DateTime dateTime = new DateTime();
         // dateTime.toDateTimeISO();
         String message = "\n[NOTIFICATION]";
         message += " [CONFIRM] [start=" + startTime + ", end=" + endTime + ", end-start=" + (endTime-startTime) + "]";

         // Writes to file
         WriteToFile wtf = new WriteToFile(mContext, file);
         wtf.writeToFile(message);
     }

    public void openAlert(View view) {
        long startTime = System.currentTimeMillis();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_confirm_title)
                .setMessage(R.string.dialog_alert_message)
                .setPositiveButton(R.string.alert_ok_message, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //
                    }
                });
        builder.show();
        long endTime = System.currentTimeMillis();

        File dir = mContext.getFilesDir();
        File file = new File(dir.getPath() + "/" + GPSTracker.FILE_NAME) ;

        String message = "\n[NOTIFICATION]";
        message += " [ALERT] [start=" + startTime + ", end=" + endTime + ", end-start=" + (endTime-startTime) + "]";

        // Writes to file
        WriteToFile wtf = new WriteToFile(mContext, file);
        wtf.writeToFile(message);

    }
}

