package kasper.argo.testapp.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.pm.ApplicationInfo;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import kasper.argo.testapp.R;
import kasper.argo.testapp.service.GPSTracker;

/**
 * Created by argokasper on 05/05/16.
 */
public class GPSActivity extends AppCompatActivity {

    GPSTracker gps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);
        if(gps == null) {
            gps = new GPSTracker(GPSActivity.this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE))
            { WebView.setWebContentsDebuggingEnabled(true); }
        }

    }

    @Override
    public void onResume(){
        super.onResume();

        if(gps == null) {
            gps = new GPSTracker(GPSActivity.this);
        }
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    public void getLocation(View view) {
        // check if GPS enabled
        if(gps.canGetLocation()){
            Location location = gps.getLocation();
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            TextView mTextView = (TextView)findViewById(R.id.current_position);
            mTextView.setText(latitude + ", " + longitude);

            // \n is for new line
            // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    public void stopLocation(View view) {
        gps.stopUsingGPS();
    }

    public void copyToClipboard(View view) {
        String label = "Locations copied";
        TextView mTextView = (TextView)findViewById(R.id.positions_file_content);
        String text = mTextView.getText().toString();
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(label, text);
        clipboard.setPrimaryClip(clip);
    }

    public void showLocation(View view) {
        /** Getting Cache Directory */
        File dir = getBaseContext().getFilesDir();

        /** Getting a reference to temporary file, if created earlier */
        File tempFile = new File(dir.getPath() + "/" + gps.FILE_NAME) ;

        /** Getting reference to edittext et_content */
        TextView posContent = (TextView) findViewById(R.id.positions_file_content);

        String strLine = "";
        StringBuilder text = new StringBuilder();

        /** Reading contents of the temporary file, if already exists */
        try {
            FileReader fReader = new FileReader(tempFile);
            BufferedReader bReader = new BufferedReader(fReader);

            /** Reading the contents of the file , line by line */
            while( (strLine=bReader.readLine()) != null  ){
                if(strLine.contains("[GPS]")) {
                    text.append(strLine + "\n");
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        /** Setting content from file */
        posContent.setText(text);
    }



}
