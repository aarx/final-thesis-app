package kasper.argo.testapp.helper;

import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by AARX on 18.05.2016.
 */
public class WriteToFile {

    Context context;
    File file;

    public WriteToFile(Context mContext, File mFile) {
        context = mContext;
        file = mFile;
    }

    public void writeToFile(String content) {

        FileWriter writer = null;
        try {
            writer = new FileWriter(file, true);

            /** Saving the contents to the file*/
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(content + "\n");
            bufferedWriter.close();

            /** Closing the writer object */
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
