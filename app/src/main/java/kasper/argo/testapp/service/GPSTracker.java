package kasper.argo.testapp.service;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;


import org.joda.time.DateTime;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import kasper.argo.testapp.helper.WriteToFile;

/**
 * Created by argokasper on 05/05/16.
 */
public class GPSTracker extends Service implements LocationListener {

    public final static String FILE_NAME = "testApp_log.txt";

    private final Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    boolean canGetLocation = false;

    // geolocation request start time
    long locationRequestStartTime = 0;

    // geolocation request end time
    long locationRequestEndTime = 0;

    Location currentLocation; // location
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 5 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 8 * 1; // 10 seconds

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context) {
        this.mContext = context;

        File dir = mContext.getFilesDir();
        File file = new File(dir.getPath() + "/" + FILE_NAME);

        DateTime dateTime = new DateTime();
        dateTime.toDateTimeISO();
        String message = "\n[GPS]";
        message += " [" + dateTime.toDateTimeISO() + "] GPS tracking started! ***";

        // Writes to file
        WriteToFile wtf = new WriteToFile(mContext, file);
        wtf.writeToFile(message);

        getLocation();
    }

    public Location getLocation() {
        locationRequestStartTime = System.currentTimeMillis();

        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                Log.d("d", "Not enabled");
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        currentLocation = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (currentLocation != null) {
                            latitude = currentLocation.getLatitude();
                            longitude = currentLocation.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (currentLocation == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            currentLocation = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (currentLocation != null) {
                                latitude = currentLocation.getLatitude();
                                longitude = currentLocation.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        locationRequestEndTime = System.currentTimeMillis();
        /*
        File cDir = mContext.getFilesDir();

        File file = new File(cDir.getPath() + "/" + FILE_NAME) ;
        String message = "\n[GPS]";
        message += " [WITH TIMESTAMPS] [lat:" + currentLocation.getLatitude() + ", lon:" + currentLocation.getLongitude()
                + ", accuracy=" + currentLocation.getAccuracy()
                + ", requestedTime: " + locationRequestStartTime + ", receivedTime: " + locationRequestEndTime + ", receivedTime-requestedTime=" + (locationRequestEndTime-locationRequestStartTime) + "]";

        // Writes to file
        WriteToFile wtf = new WriteToFile(mContext, file);
        wtf.writeToFile(message);
        */
        return currentLocation;
    }


    /**
     * Function to get latitude
     * */
    public double getLatitude() {
        if (currentLocation != null) {
            latitude = currentLocation.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude() {
        if (currentLocation != null) {
            longitude = currentLocation.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check if best network provider
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
/*
        File dir = mContext.getFilesDir();
        File file = new File(dir.getPath() + "/" + FILE_NAME);


        DateTime dateTime = new DateTime();
        dateTime.toDateTimeISO();
        String message = "\n[GPS]";
        message += " [" + dateTime.toDateTimeISO() + "] GPS tracking stopped! ***";

        // Writes to file
        WriteToFile wtf = new WriteToFile(mContext, file);
        wtf.writeToFile(message);*/

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
    /*
        File cDir = mContext.getFilesDir();

        File file = new File(cDir.getPath() + "/" + FILE_NAME) ;
        String message = "\n[GPS]";
        message += " [lat:" + currentLocation.getLatitude() + ", lon:" + currentLocation.getLongitude()
                    + ", accuracy=" + currentLocation.getAccuracy() + "]";

        // Writes to file
        WriteToFile wtf = new WriteToFile(mContext, file);
        wtf.writeToFile(message);*/
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
