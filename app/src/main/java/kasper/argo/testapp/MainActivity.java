package kasper.argo.testapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import net.danlew.android.joda.JodaTimeAndroid;

import kasper.argo.testapp.activity.CameraActivity;
import kasper.argo.testapp.activity.GPSActivity;
import kasper.argo.testapp.activity.LogActivity;
import kasper.argo.testapp.activity.NotificationActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        JodaTimeAndroid.init(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        setContentView(R.layout.activity_main);

    }

    public void openGPS(View view) {
        Intent intent = new Intent(this, GPSActivity.class);
        startActivity(intent);
    }

    public void openCamera(View view) {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    public void openNotification(View view) {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
    }

    public void openLog(View view) {
        Intent intent = new Intent(this, LogActivity.class);
        startActivity(intent);
    }
}
